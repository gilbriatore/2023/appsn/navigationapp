package br.edu.up.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class SegundaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segunda)
    }

    fun nextActivity(view: View) {
        val intent = Intent(this,TerceiraActivity::class.java)
        startActivity(intent)
    }
}